# -*- coding: utf-8 -*-

# Copyright 2012-2016 Mir Calculate. http://www.calculate-linux.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
from os import path
from calculate.core.datavars import DataVarsCore
from calculate.core.server.gen_pid import search_worked_process
from calculate.lib.cl_template import SystemIni
from calculate.lib.utils.content import getCfgFiles
from calculate.lib.utils.files import getRunCommands, readFile, writeFile


class UpdateInfo():
    """
    Информационный объект о процессе обновления
    """
    update_file = "/var/lib/calculate/calculate-update/updates.available"

    def __init__(self, dv=None):
        if dv is None:
            self.dv = DataVarsCore()
            self.dv.importCore()
        else:
            self.dv = dv

    def need_update(self):
        return self.update_ready()

    @classmethod
    def update_ready(cls):
        """
        Проверить есть ли обновления по ini.env
        """
        return path.exists(cls.update_file)

    @classmethod
    def set_update_ready(cls, value):
        """
        Установить статус обновления
        """
        try:
            if value:
                writeFile(cls.update_file).close()
            else:
                os.unlink(cls.update_file)
        except OSError:
            pass

    def check_for_dispatch(self):
        """
        Есть ли в системе не примененные файлы для dispatch-conf
        """
        return bool(getCfgFiles(self.dv.Get('cl_config_protect'),
                                prefix=self.dv.Get('cl_chroot_path')))

    def is_console_gui_run(self):
        """
        Проверить есть ли уже запущенная копия console-gui
        """
        return any([x for x in getRunCommands() if "cl-console-gui" in x])

    def update_already_run(self):
        """
        В системе уже есть работающий процесс обновления
        """
        return search_worked_process("update", self.dv)

    @property
    def outdated_kernel(self):
        flag_path = self.dv.Get('update.cl_update_outdated_kernel_path')
        if path.exists(flag_path):
            try:
                flag_kernel = readFile(flag_path).strip()
                current_kernel = self.dv.Get('update.cl_update_kernel_version')
                if flag_kernel != current_kernel:
                    return True
            except ValueError:
                pass
        return False

    @outdated_kernel.setter
    def outdated_kernel(self, value):
        flag_path = self.dv.Get('update.cl_update_outdated_kernel_path')
        try:
            if value:
                with writeFile(flag_path) as f:
                    f.write(self.dv.Get('update.cl_update_kernel_version'))
            else:
                if path.exists(flag_path):
                    os.unlink(flag_path)
        except IOError:
            pass
