# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action, Tasks
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.cl_template import TemplatesError
from calculate.lib.utils.binhosts import BinhostError
from calculate.lib.utils.files import FilesError
from ..update import UpdateError
from calculate.lib.utils.git import GitError

_ = lambda x: x
setLocalTranslate('cl_update3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClUpdateProfileAction(Action):
    """
    Действие обновление конфигурационных файлов
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError,
                    TemplatesError, BinhostError,
                    UpdateError, GitError)

    successMessage = __("The profile was successfully updated")
    failedMessage = __("Failed to update the profile")
    interruptMessage = __("Profile update manually interrupted")

    # список задач для действия
    tasks = [
        {'name': 'migrate_repository',
         'method': 'Update.migrateCacheRepository('
                   'cl_update_profile_url,cl_update_profile_branch,'
                   'cl_update_profile_storage)',
         'condition': lambda Get: Get('cl_update_profile_url')},
        {'name': 'profile_from_url',
         'group': __('setting up from url'),
         'tasks': [
            {'message': __("Repository transfer"),
             'condition': lambda Get: not (
                 Get('cl_update_profile_storage').is_local(
                     Get('cl_update_profile_url'),
                     Get('cl_update_profile_branch'))) and Get('cl_update_profile_check_sync_allowed'),
             },
            {'name': 'reconfigure_vars1',
             'method': 'Update.invalidateVariables("cl_update_profile_storage")',
             },
            {'name': 'check_datavars',
             'error': _("Profile not found in master"),
             'condition': lambda Get: not Get('update.cl_update_profile_datavars'),
             },
            {'name': 'drop_binhosts',
             'method': 'Update.drop_binhosts(update.cl_update_profile_datavars)',
             },
            {'name': 'reconfigure_vars',
             'method': 'Update.reconfigureProfileVars(cl_update_profile_datavars,'
                              'cl_chroot_path)',
             },
         ],
         'depend': Tasks.has('migrate_repository')
         },
        {'name': 'reps_synchronization',
         'group': __("Setting up the profile"),
         'tasks': [
             {'name': 'set_profile',
              'message': __("Switching to profile {cl_update_profile_system}"),
              'method': 'Update.setProfile(cl_update_profile_system)'
              },
             {'name': 'rename_custom',
              'method': 'Update.rename_custom_files()',
             },
             {'name': 'revision',
              'message': __("Fixing the settings"),
              'method': 'Update.applyProfileTemplates(cl_template_clt_set,'
                        'True,False,"update_profile")',
              'condition': lambda Get: Get('cl_templates_locate')
              },
             {'name': 'reconfigure',
              'message': __("The system is being configured"),
              'method': 'Update.applyProfileTemplates(cl_template_clt_set,'
                        'True,False,"merge")',
              'condition': lambda Get: (
                  Get('cl_update_templates_locate') and
                  Get('cl_update_skip_setup_set') == 'off')
              },
             {'name': 'dispatch_conf',
              'message': __("Updating configuration files"),
              'method': 'Update.dispatchConf()',
              'condition': lambda Get: Get('cl_dispatch_conf') != 'skip'
              },
         ]
         }
        ]
