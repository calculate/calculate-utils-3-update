# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys
from calculate.core.server.func import Action
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate
from calculate.lib.utils.files import FilesError
from ..update import UpdateError
from calculate.lib.utils.git import GitError

_ = lambda x: x
setLocalTranslate('cl_update3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)


class ClSetupUpdateAction(Action):
    """
    Действие для настройки параметров автопроверки обновлений
    """
    # ошибки, которые отображаются без подробностей
    native_error = (FilesError, UpdateError, GitError)

    successMessage = __("Updates autocheck configured!")
    failedMessage = __("Failed to configure the updates autocheck procedure!")
    interruptMessage = __("Configuration manually interrupted")

    # список задач для действия
    tasks = [
        {'name': 'set_variables',
         'method': 'Update.setAutocheckParams(cl_update_autocheck_set,'
                   'cl_update_autocheck_interval,'
                   'cl_update_other_set,'
                   'cl_update_cleanpkg_set)'}
    ]
