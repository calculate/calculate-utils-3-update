# -*- coding: utf-8 -*-

# Copyright 2010-2016 Mir Calculate. http://www.calculate-linux.org
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import sys

from calculate.lib.datavars import VariableError,DataVarsError

from calculate.core.server.func import WsdlBase
from calculate.install.install import InstallError
from .update import Update, UpdateError
from calculate.lib.utils.git import GitError
from .utils.cl_update import ClUpdateAction
from .utils.cl_update_profile import ClUpdateProfileAction
from .utils.cl_setup_update import ClSetupUpdateAction
from calculate.lib.cl_lang import setLocalTranslate, getLazyLocalTranslate, _

setLocalTranslate('cl_update3', sys.modules[__name__])
__ = getLazyLocalTranslate(_)



class Wsdl(WsdlBase):
    methods = [
        #
        # Обновить текущую конфигурацию системы (world,ревизия)
        #
        {
            # идентификатор метода
            'method_name': "update",
            # категория метода
            'category': __('Update '),
            # заголовок метода
            'title': __("Update the System"),
            # иконка для графической консоли
            'image': 'calculate-update',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-update',
            # права для запуска метода
            'rights': ['update'],
            # объект содержащий модули для действия
            'logic': {'Update': Update},
            # описание действия
            'action': ClUpdateAction,
            # объект переменных
            'datavars': "update",
            'native_error': (VariableError, DataVarsError,
                             InstallError, UpdateError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'sync'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Update the system"),
                                    normal=(
                                        'cl_update_binhost_stable_opt_set',
                                        'cl_update_binhost_recheck_set',
                                        'cl_update_with_bdeps_opt_set',
                                        'cl_update_binhost_choice',
                                        'cl_update_rep_hosting_choice',
                                    ),
                                    expert=(
                                        'cl_update_other_set',
                                        'cl_update_sync_only_set',
                                        'cl_update_pretend_set',
                                        'cl_update_sync_rep',
                                        'cl_update_emergelist_set',
                                        'cl_update_check_rep_set',
                                        'cl_update_force_fix_set',
                                        'cl_update_ignore_level',
                                        'cl_update_force_level',
                                        'cl_update_world',
                                        'cl_update_gpg_force',
                                        'cl_update_egencache_force',
                                        'cl_update_eixupdate_force',
                                        'cl_update_revdep_rebuild_set',
                                        'cl_update_wait_another_set',
                                        'cl_update_autocheck_schedule_set',
                                        'cl_update_onedepth_set',
                                        'cl_update_cleanpkg_set',
                                        'cl_update_branch_data',
                                        'cl_templates_locate',
                                        'cl_verbose_set', 'cl_dispatch_conf'),
                                    next_label=_("Run"))]},
        #
        # Сменить профиль
        #
        {
            # идентификатор метода
            'method_name': "update_profile",
            # категория метода
            'category': __('Update '),
            # заголовок метода
            'title': __("Change the Profile"),
            # иконка для графической консоли
            'image': 'calculate-update-profile,'
                     'notification-display-brightness-full,gtk-dialog-info,'
                     'help-hint',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-update-profile',
            # права для запуска метода
            'rights': ['change_profile'],
            # объект содержащий модули для действия
            'logic': {'Update': Update},
            # описание действия
            'action': ClUpdateProfileAction,
            # объект переменных
            'datavars': "update",
            'native_error': (VariableError, DataVarsError,
                             InstallError, UpdateError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'update_profile', 'cl_update_world_default': "rebuild"},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Repository"),
                                    brief=('cl_update_profile_repo_name',),
                                    hide=('cl_update_profile_url',
                                          'cl_update_profile_sync_set'),
                                    normal=('cl_update_profile_url',),
                                    expert=('cl_update_profile_sync_set',)),
                lambda group: group(_("Profile"),
                                    normal=('cl_update_profile_system',
                                            'cl_update_world'),
                                    expert=('cl_update_skip_setup_set',
                                            'cl_update_templates_locate',
                                            'cl_verbose_set',
                                            'cl_dispatch_conf'),
                                    hide=('cl_update_templates_locate',
                                            'cl_verbose_set',
                                            'cl_dispatch_conf'),
                                    brief=('cl_update_profile_system',
                                           'cl_update_profile_linux_fullname',
                                           'cl_update_profile_depend_data')
                                    )],
            'brief': {'next': __("Run"),
                      'name': __("Set the profile")}},
        #
        # Настроить автопроверку обновлений
        #
        {
            # идентификатор метода
            'method_name': "setup_update",
            # категория метода
            'category': __('Configuration'),
            # заголовок метода
            'title': __("Update Check"),
            # иконка для графической консоли
            'image': 'calculate-setup-update,software-properties,preferences-desktop',
            # метод присутствует в графической консоли
            'gui': True,
            # консольная команда
            'command': 'cl-setup-update',
            # права для запуска метода
            'rights': ['setup_update'],
            # объект содержащий модули для действия
            'logic': {'Update': Update},
            # описание действия
            'action': ClSetupUpdateAction,
            # объект переменных
            'datavars': "update",
            'native_error': (VariableError, DataVarsError,
                             InstallError, UpdateError, GitError),
            # значения по умолчанию для переменных этого метода
            'setvars': {'cl_action!': 'merge'},
            # описание груп (список лямбда функций)
            'groups': [
                lambda group: group(_("Updates autocheck settings"),
                                    normal=('cl_update_autocheck_set',
                                            'cl_update_autocheck_interval',
                                            'cl_update_cleanpkg_set',
                                            'cl_update_other_set'),
                                    next_label=_("Save"))]},
    ]
